define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/UserModel',
    'text!app/templates/AdminPanel/NewUserTemplate.html',
    'app/collections/AdminCollections/ControlUsername',


], function($, _,Backbone,Handlebars,UserModel,NewUserTemplate,ControlUsername){

    var NewUser=Backbone.View.extend({

        el:'.page',
        events:{
          'click #new':'new',
          'click #notNew':'notNew',
          'change #newUserkapali':'close',
          'change #newUseracik':'open',
            'change  #newUserhaftalik':'changeWeek',
            'change  #newUseraylik':'changeMonth',

        },
        changeMonth:function () {

            $("#newUserhaftalik").prop( "checked", false );

        },
        changeWeek:function () {

            $("#newUseraylik").prop( "checked", false );

        },

        close:function () {

debugger
            $("#ac").prop( "checked", false );
            $("#newUseracik").toggle();
            return false;


        },
        open:function () {
debugger
            $("#kapa").prop( "checked", false );
            $("#newUserkapali").toggle();

            $("#seyahat").toggle();
            $("#newUseraylik").prop( "checked", true );
            return false;

        },

        new:function () {
            var username=$("#username").val();
        var informationSystem;
        var user=new UserModel();
        user.set("username",$("#username").val());
        user.set('sicilNo',$("#sicilNo").val());
        user.set('role',$("#role").val());
        user.set('email',$("#email").val());
        user.set('password',$("#password").val());
        user.set('department',$("#department").val());
        user.set('departmentManager',$("#departmentManager").val());
        user.set("firstLastName",$("#firstLastName").val());

            if(!user.isValid())
            {
                $("." + user.validationError).tooltip("show");


            }else{

                var control=new ControlUsername({
                    username:username,


                });
                control.fetch({
                    success:function (m_user) {
                        if(!m_user.size()==0)
                        {

                            $('#ornekModal').modal('show');
                            return false;



                        }


                        else {
                            if($("#kapa").is(':checked'))
                            {
                                informationSystem=$("#kapa").val();

                            }
                            else if($("#ac").is(':checked')){

                                if($("#newUseraylik").is(':checked')){



                                    informationSystem= $("#newUseraylik").val()+$("#gunler").val();


                                }
                                else if($("#newUserhaftalik").is(':checked')){


                                    informationSystem=$("#newUserhaftalik").val()+$("#gunler").val();


                                }


                            }
                            else{

                                informationSystem="-";
                            }




                            user.set('informationSystem',informationSystem);
                            user.save();
                            window.location.href='#/users';
                            return false;




                        }

                    }

                });

                return false;


            }




        return false;
        },
        notNew:function () {
            window.location.href='#/users';
            return false;
        },
        render:function () {

            this.$el.html(NewUserTemplate);
            return this;


        }

    });


    return NewUser;



});
