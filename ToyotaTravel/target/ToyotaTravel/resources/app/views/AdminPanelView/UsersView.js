define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/UserModel',
    'app/models/LoginModel',
    'text!app/templates/AdminPanel/UsersTemplate.html',
    'text!app/templates/AdminPanel/UsersListTemplate.html'


], function($, _,Backbone,Handlebars,UserModel,LoginModel,UserTemplate,UserListTemplate){


        var UsersEventView=Backbone.View.extend({
            tagName:'tr',
            model:UserModel,
            events: {
               'click .deleteUser': 'deleteUser',
                'mouseover .resultRow':'changeColor',
                'mouseout .resultRow':'notChangeColor'
            },
            notChangeColor:function () {
                this.$el.css('color', 'black');

            },
            changeColor:function () {

                this.$el.css('color', 'red');
            },
            deleteUser: function () {
                var that=this;
                if(uyar())
                {


                    console.log(that.model.toJSON().username)
                    //this.$el.find(".deleteUser").addClass("disabled");
                    var loginModel=new LoginModel();
                    loginModel.fetch({
                        success:function (m_user) {
                            if(m_user.toJSON().username==that.model.toJSON().username)
                            {

                                alert("Admin Kendini Silemez!!! ");

                                return false;
                            }
                            else{

                                this.model.destroy({
                                    wait: true,
                                    success: function () {
                                        that.remove();

                                    },

                                });

                                return false;
                            }
                        }

                    });



                    }   else{

                    return false;
                }

            },
            render: function () {
                var that = this;
                var template = Handlebars.compile(UserListTemplate);
                var myHtml = template(that.model.toJSON());
                that.$el.html(myHtml);
                return this;
            }




        });

        var UsersView =Backbone.View.extend({
            el: $(".page"),

            render: function () {
                this.$el.html(UserTemplate);
                return this;
            }

        });





    return {
        UsersEventView: UsersEventView,
        UsersView: UsersView
    };



});
