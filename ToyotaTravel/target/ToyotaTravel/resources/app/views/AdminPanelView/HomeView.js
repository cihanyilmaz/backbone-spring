define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/TravelModel',
    'text!app/templates/AdminPanel/HomeTemplate.html',
    'text!app/templates/AdminPanel/HomeListTemplate.html',
    'app/collections/AdminCollections/TravelSearchCollection',
    'app/collections/AdminCollections/UsernameSearchCollection',
    'app/views/EmptyTravelView'


], function($, _,Backbone,Handlebars,TravelModel, HomeTemplate,HomeListTemplate,TravelSearchCollection,

            UsernameSearchCollection,EmptyTravelView){


    var HomeEventView=Backbone.View.extend({
        tagName:'tr',
        model:TravelModel,
        events: {
            'click .editTravel': 'editTravel',
            'click .updateTravel': 'updateTravel',
            'click .cancelEdit': 'cancelEdit',
            'click .deleteTravel': 'deleteTravel',
            'mouseover .resultRow':'changeColor',
            'mouseout .resultRow':'notChangeColor'
        },
        notChangeColor:function () {
            this.$el.css('color', 'black');

        },
        changeColor:function () {

            this.$el.css('color', 'red');
        },
        editTravel: function () {
            this.$el.addClass("info");
            this.$el.find("input").show().focus();
            this.$el.find("span").hide();
            $(".editTravel").hide();
            $(".deleteTravel").hide();
            this.$el.find(".updateTravel").show();
            this.$el.find(".cancelEdit").show();
        },
        cancelEdit: function () {
            this.$el.removeClass("info");
            $(".editTravel").show();
            $(".deleteTravel").show();
            this.render();
        },
        updateTravel: function () {

            var department = this.$el.find("#department").val();
            var departmentManager = this.$el.find("#departmentManager").val();
            var beginTravel = this.$el.find("#beginTravel").val();
            var endTravel = this.$el.find("#endTravel").val();
            var travelPlace = this.$el.find("#travelPlace").val();
            var goPurpose = this.$el.find("#goPurpose").val();
            var projectCode = this.$el.find("#projectCode").val();
            var travelCost = this.$el.find("#travelCost").val();
            var username = this.$el.find("#username").val();

            var newValues=new TravelModel();
            newValues.set("department",department);
            newValues.set("departmentManager",departmentManager);
            newValues.set("beginTravel",beginTravel);
            newValues.set("endTravel",endTravel);
            newValues.set("travelPlace",travelPlace);
            newValues.set("goPurpose",goPurpose);
            newValues.set("projectCode",projectCode);
            newValues.set("travelCost",travelCost);

            this.$el.removeClass("info");
            if (!newValues.isValid()){
                if(newValues.validationError=="begin")
                {
                    $("#beginTravel").attr("title","Başlangıç tarihi, Seyehat Sonu tarihinden büyük olamaz!!");
                    $("#beginTravel").tooltip("show");
                    $("#beginTravel").attr("title", "Boş Geçilemez!!");
                    return false;
                }
                $("." + newValues.validationError).tooltip("show");
                setTimeout(function () {
                    $("." + newValues.validationError).tooltip("destroy");
                }, 1000);

            }else{

                this.$el.removeClass("info");
                this.model.set("department", department);
                this.model.set("departmentManager", departmentManager);
                this.model.set("beginTravel", beginTravel);
                this.model.set("endTravel", endTravel);
                this.model.set("travelPlace",travelPlace);
                this.model.set("goPurpose", goPurpose);
                this.model.set("projectCode", projectCode);
                this.model.set("travelCost", travelCost);
                this.model.set("username", username);

                this.model.save();

                $(".editTravel").show();
                $(".deleteTravel").show();
                this.render();


            }

        },
        deleteTravel: function () {
            var that=this;


            if(uyar())
            {
                this.$el.find(".deleteTravel").addClass("disabled");
                var that = this;

                this.model.destroy({
                    wait: true,success: function () {
                        that.remove();

                    },

                }); // HTTP DELETE

            }else{

                return false;
            }








        },
        render: function () {
            var that = this;
            var template = Handlebars.compile(HomeListTemplate);
            var myHtml = template(that.model.toJSON());
            that.$el.html(myHtml);
            return this;
        }




    });
    var HomeView=Backbone.View.extend({

        el: $(".page"),
        events:{
            'click #searchTravel': 'search',
            'change #homeUsers':'searchTravels',
            'click #more':'more',


        },
        more:function () {
            var topla=8;//9 yazınca 3lü atıyor
            debugger
            parseInt(sessionStorage.setItem("degeri",topla+  parseInt(sessionStorage.getItem("degeri"))))

            $("#travelList tr:lt("+sessionStorage.getItem("degeri")+")").show();

        },
        search:function () {

            debugger
            if($("#beginDate").val()=='' && $("#endDate").val()=='' )
                location.reload();

            if($("#beginDate").val()=='')
            {
                $(".adminBeginDate").tooltip("show");
                setTimeout(function () {
                    $(".adminBeginDate").tooltip("destroy");
                }, 2000);
                return false;
            }
            if($("#endDate").val()=='')
            {
                $(".adminEndDate").tooltip("show");
                return false;
            }

            if($("#beginDate").val()>$("#endDate").val())
            {
                $(".adminBeginDate").attr("title","Başlangıç Tarihi Bitiş tarihinden Büyük Olamaz!!");
                $(".adminBeginDate").tooltip("show");
                $(".adminBeginDate").attr("title","Boş Geçilemez!!");
                setTimeout(function () {
                    $(".adminBeginDate").tooltip("destroy");
                }, 2000);

                return false;
            }


            var beginDate = $("#beginDate").val();
            var endDate = $("#endDate").val();

            var search = new TravelSearchCollection( {
                beginDate: beginDate,
                endDate: endDate,

            });

            search.fetch({

                success: function (m_travels) {

                    $(".resultRow").parent().empty();

                    if (m_travels.size() == 0) {
                        var emptyTravelView=new EmptyTravelView();

                        emptyTravelView.model={column:10};

                        $("#travelList").append(emptyTravelView.render().el);

                    }else{

                        m_travels.each(function (m_travel) {

                            var searchView = new HomeEventView();
                            searchView.model = m_travel;
                            $("#travelList").append(searchView.render().el);
                        });


                    }





                }
            });


        },
        searchTravels:function () {

            $("#beginDate").val("");
            $("#endDate").val("");

            var username=$("#homeUsers :selected").text();

            if($("#homeUsers :selected").val()=="0")
            {
                location.reload();
            }

            var searchUsername=new UsernameSearchCollection({

                username:username

            });

            searchUsername.fetch({
                success:function (m_travels) {
                    $(".resultRow").parent().empty();

                    if (m_travels.size() == 0) {
                        var emptyTravelView=new EmptyTravelView();

                        emptyTravelView.model={column:10};

                        $("#travelList").append(emptyTravelView.render().el);

                    }else{

                        m_travels.each(function (m_travel) {

                            var adminHomeView=new HomeEventView();
                            adminHomeView.model=m_travel;
                            $("#travelList").append(adminHomeView.render().el);


                        })
                    }

                }

            });


        },

        render: function () {
            this.$el.html(HomeTemplate);
            return this;
        }
    });


    return {
        HomeEventView: HomeEventView,
        HomeView: HomeView
    };




});
