define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/UserModel',
    'text!app/templates/AdminPanel/NewTravelTemplate.html',
    'app/models/TravelModel',



], function($, _,Backbone,Handlebars,UserModel,NewTravelTemplate,TravelModel){


    var NewTravelView=Backbone.View.extend({

        el:'.page',
        events:{
            'click #saveTravel':'saveTravel',
            'click #notSaveTravel':'notSaveTravel',

        },


        saveTravel:function () {

            var travel =new TravelModel();

            var user=new UserModel({id:$("#travelUsers").val()});
            travel.set("username",$("#travelUsers").val());
            travel.set("travelPlace",$("#travelPlace").val());
            travel.set("goPurpose",$("#goPurpose").val());
            travel.set("beginTravel",$("#beginTravel").val());
            travel.set("endTravel",$("#endTravel").val());
            travel.set("travelCost",$("#travelCost").val());
            travel.set("projectCode",$("#projectCode").val());


            if(!travel.isValid())
            {


                if(travel.validationError=="begin")
                {
                    $("#beginTravel").attr("title","Başlangıç tarihi, Seyehat Sonu tarihinden büyük olamaz!!");
                     $("#beginTravel").tooltip("show");
                    $("#beginTravel").attr("title", "Boş Geçilemez!!");
                    return false;
                }

                $("." + travel.validationError).tooltip("show");
                   // $("#beginTravel").attr("title", travel.validationError);
                  //  $("#beginTravel").tooltip("show");
                  //  $("#beginTravel").attr("title", "Boş Geçilemez!!");


                setTimeout(function () {

                    $("#beginTravel").tooltip("destroy");
                }, 2000);

                return false;

            }else{
              user.fetch({

                                success:function (m_user) {

                                    travel.set("department",m_user.toJSON().department)
                                    travel.set("departmentManager",m_user.toJSON().departmentManager);

                                    travel.set("username",m_user.toJSON().username)
                                    debugger
                                    travel.save();
                                    window.location.href='#/adminPanel';
                                    return false;
                                }

                            });


                return false;
            }




        return false;
        },
        notSaveTravel:function () {

          window.location.href='#/adminPanel';


        },
        render:function () {

            this.$el.html(NewTravelTemplate);
            return this;
        }

    });

    return NewTravelView;





});
