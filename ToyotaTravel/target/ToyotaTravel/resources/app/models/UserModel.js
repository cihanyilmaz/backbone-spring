define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
], function($, _, Backbone,Handlebars){



    var UserModel=Backbone.Model.extend({

        urlRoot:'/rest/user',
        validate:function (ev) {
            if(ev.username.trim()==""){
                return "usernameAdd";
            }

            if(ev.password.trim()=="")
            {
                return "passwordAdd";
            }
            if(ev.sicilNo=="")
            {
                return "sicilNoAdd";
            }
            if(ev.department.trim()=="")
            {
                return "departmentAdd";
            }
            if(ev.departmentManager.trim()=="")
            {
                return "departmentManagerAdd";
            }

        }

    });

    return UserModel;



});


