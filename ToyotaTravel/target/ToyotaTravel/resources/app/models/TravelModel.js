define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
], function($, _, Backbone,Handlebars){



    var TravelModel=Backbone.Model.extend({

        urlRoot:'/rest/travel',
        validate:function (ev) {
        if(ev.username==0)
        {
            return "usernameAdd";
        }
        if(ev.travelPlace.trim()=="")
        {
            return "travelPlaceAdd";
        }
        if(ev.goPurpose.trim()=="")
        {
            return "goPurposeAdd";
        }
        if(ev.projectCode.trim()==""){

            return "projectCodeAdd";
        }
        if(ev.travelCost==""){

            return "travelCostAdd";
        }
        if(ev.beginTravel=="")
        {
            return "beginTravelAdd";
        }
        if(ev.endTravel=="")
        {
        return "endTravelAdd";
        }

        if(ev.beginTravel>ev.endTravel)
        {
            return "begin";
        }

        }

    });

    return TravelModel;



});

