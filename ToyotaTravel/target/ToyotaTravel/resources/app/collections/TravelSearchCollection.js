define(['jquery', 'underscore', 'backbone', 'app/models/TravelModel'], function ($, _, Backbone, TravelModel) {


    var Search = Backbone.Collection.extend({

        model: TravelModel,

        initialize: function (options) {
            debugger
            this.beginDate = options.beginDate;
            this.endDate=options.endDate;

        },
        url:function () {


            return "rest/travel/search/"
                + this.beginDate + "/" + this.endDate;

        }




    });



    return Search;

});
