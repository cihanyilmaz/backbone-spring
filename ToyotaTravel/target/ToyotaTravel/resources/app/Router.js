define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'moment',
    'spin',
    'app/views/UserPanelView/HomeView',
    'app/views/LoginView',
    'app/models/LoginModel',
    'app/collections/TravelCollection',
    'app/views/HomeView',
    'app/views/UserPanelView/NewTravelView',
    'app/models/UserModel',
    'app/views/UserPanelView/EditUserView',
    'app/views/AdminPanelView/HomeView',
    'app/collections/AdminCollections/TravelCollection',
    'app/views/AdminPanelView/EditAdminView',
    'app/views/AdminPanelView/UsersView',
    'app/collections/AdminCollections/UsersCollection',
    'app/views/AdminPanelView/NewUserView',
    'app/views/AdminPanelView/NewTravelView',
    'app/views/AdminPanelView/SelectUsersView',
    'app/views/EmptyTravelView'
    


], function($, _,Backbone,Handlebars,moment,Spinner,UserHomeView,LoginView,LoginModel,TravelCollection,HomeView

    ,NewTravelView,UserModel,EditUserView,AdminHomeView,AdminTravelCollection,EditAdminView,UsersView,UsersCollection,NewUserView

      ,NewAdminTravelView,SelectUsersView,EmptyTravelView){

    var Router = Backbone.Router.extend({

        routes: {
            '': 'home',
            'login':'login',
            'panel':'panel',
            'userPanel':'userPanel',
            'newTravel':'newTravel',
            'editUser/:id':'editUser',
            'adminPanel':'adminPanel',
            'editAdmin/:id':'editAdmin',
            'users':'users',
            'newUser':'newUser',
            'newAdminTravel':'newAdminTravel'
           

        },
        initialize: function () {

            this.homeView=new HomeView();
            this.loginView=new LoginView();
            this.userView=new UserHomeView.HomeView();
            this.newTravelView=new NewTravelView();
            this.editUserView=new EditUserView();
            this.newAdminTravelView=new NewAdminTravelView();
            this.newUserView=new NewUserView();
            this.usersView=new UsersView.UsersView();
            this.editAdminView=new EditAdminView();
            this.adminHomeView=new AdminHomeView.HomeView();
            sessionStorage.setItem("degeri",0);

        },

        home:function () {
    // var homeView=new HomeView();
      // homeView.render();
            var that=this;
           that.homeView.render();
        },

        login:function () {
        //   var loginView=new LoginView();
         // loginView.render();

            var that=this;
           that.loginView.render();
            if (getParameterByName("error") != 1) {
                $("#errorLogin").tooltip("destroy");
            }
            //Eğer hatalı deneme yapıldıysa hatayı görünür yap.
            else {
                $("#errorLogin").attr("title", "Hatalı Giriş Yaptınız!!!")
                $("#errorLogin").tooltip("show");
                setTimeout(function () {
                    $("#userLoginButton").tooltip("destroy");
                }, 1000);
                $("#errorLogin").attr("title", "");
            }

        },
        panel:function () {

            var loginModel=new LoginModel();

            loginModel.fetch({

                success:function (m_loginModel) {
                    if(m_loginModel.toJSON().role=="Admin")
                        window.location.href = '#/adminPanel';
                    else if(m_loginModel.toJSON().role=="User")
                        window.location.href = '#/userPanel';

                    else
                        window.location.href = '#/login';


                }


            });

        },
        userPanel:function () {
            Handlebars.registerHelper('formatTime', function (date, format) {
                var mmnt = moment(date);
                return mmnt.format(format);
            });


            //Sayfa ilk açıldığında veri giriş alanları ve tablo render ediliyor.
          // var userPanel= new UserHomeView.HomeView();
           // userPanel.render();

          var that=this;
            that.userView.render();


            var loginUser=new LoginModel();
            loginUser.fetch({
                success:function (m_loginUser) {
                    $("#userButton").append(m_loginUser.toJSON().firstLastName);
                    $('#userButton').attr('href', '#/editUser/'+m_loginUser.toJSON().id);

                }


            });

            var spinner =new Spinner();
            $('body').after(spinner.spin().el); //body
            var travelCollection=new TravelCollection();
            travelCollection.fetch({

                success:function (m_travels) {

                    spinner.stop();
                    if (m_travels.size() == 0) {
                        var emptyTravelView=new EmptyTravelView();

                        emptyTravelView.model={column:8};

                        $("#travelList").append(emptyTravelView.render().el);

                    }else{

                        m_travels.each(function (m_travel) {
                            debugger
                            var travelView=new UserHomeView.HomeEventView();
                            travelView.model=m_travel;

                            // that.travelView.model=m_travel;
                            $("#travelList").append(travelView.render().el);
                           // $("#travelList td:gt(47)").hide();//47

                            $("#travelList tr:gt(6)").hide();



                        })

                    }



                }

            });

        },

        newTravel:function () {
        var that=this;
        that.newTravelView.render();
            //  var newTravelView=new NewTravelView();
            // newTravelView.render();
          //  disposeView(new NewTravelView().render());
        },
        editUser:function (id) {
            var that =this;
            var user=new UserModel({id:id});

            user.fetch({
                success:function (m_user) {
                //  var editUserView=new EditUserView();
                //  editUserView.model=m_user;
                //  editUserView.render();

                   that.editUserView.model=m_user;
                  that.editUserView.render();
                }

            });



        },

        adminPanel:function () {
            Handlebars.registerHelper('formatTime', function (date, format) {
                var mmnt = moment(date);
                return mmnt.format(format);
            });


        // var adminPanel=  new AdminHomeView.HomeView();
         // adminPanel.render();
            var that=this;
            that.adminHomeView.render();
           var listUsers=new SelectUsersView.SelectUsersList();

            SelectUsersView.userCollection.fetch({
                success:function (m_users) {

                    listUsers.render(m_users);
                }



            });



            var loginUser=new LoginModel();
            loginUser.fetch({
                success:function (m_loginUser) {
                    $("#adminButton").append(m_loginUser.toJSON().firstLastName);
                    $('#adminButton').attr('href', '#/editAdmin/'+m_loginUser.toJSON().id);

                }


            });

            var spinner =new Spinner();
            $('body').after(spinner.spin().el);

            var adminTravels=new AdminTravelCollection();

            adminTravels.fetch({

                success:function (m_travels) {
                    spinner.stop();
                    if (m_travels.size() == 0) {
                        var emptyTravelView=new EmptyTravelView();

                        emptyTravelView.model={column:10};

                        $("#travelList").append(emptyTravelView.render().el);

                    }else{
                            debugger
                        m_travels.each(function (m_travel) {
                            var travelView=new AdminHomeView.HomeEventView();
                            travelView.model=m_travel;

                            $("#travelList").append(travelView.render().el);
                            $("#travelList tr:gt(5)").hide();//47

                        })

                    }


                }

            });
        },
        editAdmin:function (id) {
            var that=this;
            var user=new UserModel({id:id});

            user.fetch({
                success:function (m_user) {
                  //  var editAdminView=new EditAdminView();
                  //  editAdminView.model=m_user;
                  //  editAdminView.render({id:id});

                 that.editAdminView.model=m_user;
                 that.editAdminView.render();

                }

            });


        },
        users:function () {
            var that=this;
            var spinner =new Spinner();
            $('body').after(spinner.spin().el);

           //var usersView=new UsersView.UsersView();
            that.usersView.render();
           // var that=this;
          // usersView.render();
            var loginUser=new LoginModel();
            loginUser.fetch({
                success:function (m_loginUser) {

                    $("#adminButton").append(m_loginUser.toJSON().firstLastName);
                    $('#adminButton').attr('href', '#/editAdmin/'+m_loginUser.toJSON().id);

                }
            });

            var usersCollection=new UsersCollection();

            usersCollection.fetch({
                success:function (m_users) {
                    debugger
                    spinner.stop();
                    m_users.each(function (m_user) {
                        debugger

                       var usersEventView=new UsersView.UsersEventView();
                     usersEventView.model=m_user;
                       // that.usersEventView.model=m_user;
                        $("#all").append(usersEventView.render().el);


                    })

                }

            });

        },
        newUser:function () {
            var that=this;
        //var newUserView=new NewUserView();
          that.newUserView.render();

        },
        newAdminTravel:function () {
            var that=this;
           // var newTravelView=new NewAdminTravelView();
          // newTravelView.render();
            that.newAdminTravelView.render();
            var listUsers=new SelectUsersView.SelectUsersList();

            SelectUsersView.userCollection.fetch({
               success:function (m_users) {
                 listUsers.render(m_users);
               }


            });



        }


    });

    return Router;

});