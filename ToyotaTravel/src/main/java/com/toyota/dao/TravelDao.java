package com.toyota.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.toyota.model.Login;


import com.toyota.model.User;
import com.toyota.resource.LoginResource;
import com.toyota.security.CustomAuthenticationProvider;
import com.toyota.service.UserService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.toyota.model.Travel;
@Repository
public class TravelDao {
	
	//@Autowired //@Autowired
    //private SessionFactory sessionFactory;


	@PersistenceContext
	public EntityManager entityManager;
	
	@Autowired
	private UserService userService;
	
	public Travel save(Travel travel)
	{
		//sessionFactory.getCurrentSession().save(travel);
		entityManager.persist(travel);
		return travel;
	}
	
	public void delete(int id)
	{
		
		//sessionFactory.getCurrentSession().delete(findId(id));
		entityManager.remove(findId(id));
		
	   
	}

	public void edit(Travel travel)
	{
		// sessionFactory.getCurrentSession().update(travel);
		entityManager.merge(travel);
		
	
	}
	public List<Travel> all()
	{
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		Calendar tkv = Calendar.getInstance();//�imdiki zaman
		System.out.println("şimdi"+df.format(tkv.getTime()));
		tkv.add(Calendar.MONTH,-2);

		System.out.println("2 ay önce"+df.format(tkv.getTime()));//�imdiki zaman�n iki ay �ncesi

		String begin=df.format(tkv.getTime());
		String end=df.format(new Date());
		Date i=tkv.getTime();
		Date s=new Date();
		System.out.println("Date 2 ay �nce : " + i+"  "+s);

		
		
		
		//Session session = this.sessionFactory.getCurrentSession();
        //List<Travel> musteriListe = session.createQuery("FROM Travel").list();

		//Criteria cr = session.createCriteria(Travel.class);
		 Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Travel.class);
		 criteria.add(Restrictions.eq("username",CustomAuthenticationProvider.getUsername()));
		 criteria.add(Restrictions.between("beginTravel",i ,s));
		
		List results = criteria.list();
     
        return results;
		
	}
	
	public Travel findId(int id){
		
		return entityManager.find(Travel.class, id);
				//sessionFactory.getCurrentSession().get(Travel.class, id);
		
	}
	
	public List<Travel> searchTravel(String beginDate,String endDate){
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date i = null,s = null;
		
		try {
			i = df.parse(beginDate);
			
			s =df.parse(endDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		
		//Session session = this.sessionFactory.getCurrentSession();
		// Criteria criteria = session.createCriteria(Travel.class);
		 Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Travel.class);
		 criteria.add(Restrictions.eq("username",CustomAuthenticationProvider.getUsername()));
		 criteria.add(Restrictions.ge("beginTravel", i)); 
		 criteria.add(Restrictions.lt("endTravel", s));
		 List results = criteria.list();
		return results;
		
	}
	
	public List<Travel> adminAll()
	{	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

	Calendar tkv = Calendar.getInstance();//�imdiki zaman
	System.out.println("şimdi"+df.format(tkv.getTime()));
	tkv.add(Calendar.YEAR,-1);

	System.out.println("2 ay önce"+df.format(tkv.getTime()));//�imdiki zaman�n iki ay �ncesi

	String begin=df.format(tkv.getTime());
	String end=df.format(new Date());
	Date i=tkv.getTime();
	Date s=new Date();
	System.out.println("Date 2 ay �nce : " + i+"  "+s);
		
		
		
		
		//Session session = this.sessionFactory.getCurrentSession();
        //List<Travel> musteriListe = session.createQuery("FROM Travel").list();

		//Criteria cr = session.createCriteria(Travel.class);
	 Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Travel.class);
	 criteria.add(Restrictions.between("beginTravel",i ,s));
		List results = criteria.list();
     
        return results;
		
	}
	
	public List<Travel> searchAdminTravel(String beginDate,String endDate){
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date i = null,s = null;
		
		try {
			i = df.parse(beginDate);
			
			s =df.parse(endDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		
		//Session session = this.sessionFactory.getCurrentSession();
		 //Criteria criteria = session.createCriteria(Travel.class);
		 Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Travel.class);
		 criteria.add(Restrictions.ge("beginTravel", i)); 
		 criteria.add(Restrictions.lt("endTravel", s));
		 List results = criteria.list();
		return results;
		
		
		
	}
	
	public List<Travel> searchUsername(String username){
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		Calendar tkv = Calendar.getInstance();//�imdiki zaman
		System.out.println("şimdi"+df.format(tkv.getTime()));
		tkv.add(Calendar.YEAR,-1);

		System.out.println("2 ay önce"+df.format(tkv.getTime()));//�imdiki zaman�n iki ay �ncesi

		String begin=df.format(tkv.getTime());
		String end=df.format(new Date());
		Date i=tkv.getTime();
		Date s=new Date();
		System.out.println("Date 2 ay �nce : " + i+"  "+s);
		
		
		//Session session = this.sessionFactory.getCurrentSession();
    	//Criteria cr = session.createCriteria(Travel.class);
		 Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Travel.class);
		 criteria.add(Restrictions.eq("username",username));
		 criteria.add(Restrictions.between("beginTravel",i ,s));
		List results = criteria.list();
     
        return results;
		
		
	}
	  public List<Travel> getTravels(String username) {

		  // Session session = this.sessionFactory.getCurrentSession();
			// Criteria criteria = session.createCriteria(Travel.class);
		  Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Travel.class);
		  	criteria.add(Restrictions.eq("username",username));
		     
		   List results = criteria.list();
		    return results;
		   }
	  
	  
	 
	
	
	

}
