package com.toyota.job;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;



import com.toyota.model.Travel;
import com.toyota.model.User;
import com.toyota.mail.MailMail;
import com.toyota.service.TravelService;
import com.toyota.service.UserService;

@Component
public class Job {
	@Autowired
	private UserService userService;
	
	@Autowired
	private TravelService travelService;
	
	@Autowired
	private MailMail mail ;

	public static String newline = System.getProperty("line.separator");
	
	
	@Scheduled(cron="55 * * * * *")
	public void testMethod(){
		
		/*System.out.println("metod calisti");
		List<User> users =userService.findInformation("Haftal�k-Per�embe");
		System.out.println("sayisi"+users.size());
		for(int i=0;i<users.size();i++)
		{
			String send = "";
			String username=users.get(i).getUsername();
			System.out.println("�u anki username"+username);
			List<Travel> travels=travelService.getTravels(username);
			
			for(int k=0;k<travels.size();k++)
			{
				send=send+travels.get(k).getBeginTravel()+travels.get(k).getTravelPlace()+newline;
				System.out.println(k+".ci not::"+travels.get(k).getBeginTravel());
			}
			
			mail.sendMail(users.get(i).getEmail(), "seyehatlerin", send);
		}
		*/
		
		System.out.println("�rnek �al��t�rma"+new Date());
	}
	
	@Scheduled(cron="0 0 14 1 * 1")
	public void monthMonday(){
		List<User> users =userService.findInformation("Aylik-Pazartesi");
		for(int i=0;i<users.size();i++)
		{
			String send = "";
			String username=users.get(i).getUsername();
			
			List<Travel> travels=travelService.getTravels(username);
			for(int k=0;k<travels.size();k++)
			{
				send=send+travels.get(k).getBeginTravel()+travels.get(k).getTravelPlace()+newline;
			
			}
			
			mail.sendMail(users.get(i).getEmail(), "seyehatlerin", send);
		}
		System.out.println("14: 00'da ve 1. Pazartesi g�n�."+new Date());
	}
	@Scheduled(cron="0 0 14 1 * 2")
	public void monthTuesday(){
		List<User> users =userService.findInformation("Aylik-Sali");
		for(int i=0;i<users.size();i++)
		{
			String send = "";
			String username=users.get(i).getUsername();
			
			List<Travel> travels=travelService.getTravels(username);
			for(int k=0;k<travels.size();k++)
			{
				send=send+travels.get(k).getBeginTravel()+travels.get(k).getTravelPlace()+newline;
			
			}
			
			mail.sendMail(users.get(i).getEmail(), "seyehatlerin", send);
		}
		System.out.println("14: 00'da ve 1. Sali g�n�."+new Date());
	}
	
	@Scheduled(cron="0 0 14 1 * 3")
	public void monthWednesday(){
		List<User> users =userService.findInformation("Aylik-�ar?amba");
		for(int i=0;i<users.size();i++)
		{
			String send = "";
			String username=users.get(i).getUsername();
			
			List<Travel> travels=travelService.getTravels(username);
			for(int k=0;k<travels.size();k++)
			{
				send=send+travels.get(k).getBeginTravel()+travels.get(k).getTravelPlace()+newline;
			
			}
			
			mail.sendMail(users.get(i).getEmail(), "seyehatlerin", send);
		}
		System.out.println("14: 00'da ve 1. �ar?amba g�n�."+new Date());
	}
	@Scheduled(cron="0 0 14 1 * 4")
	public void monthThursday(){
		List<User> users =userService.findInformation("Aylik-Per?embe");
		for(int i=0;i<users.size();i++)
		{
			String send = "";
			String username=users.get(i).getUsername();
			
			List<Travel> travels=travelService.getTravels(username);
			for(int k=0;k<travels.size();k++)
			{
				send=send+travels.get(k).getBeginTravel()+travels.get(k).getTravelPlace()+newline;
			
			}
			
			mail.sendMail(users.get(i).getEmail(), "seyehatlerin", send);
		}
		System.out.println("14: 00'da ve 1. per?embe g�n�."+new Date());
	}
	
	@Scheduled(cron="0 0 14 1 * 5")
	public void monthFriday(){
		List<User> users =userService.findInformation("Aylik-Cuma");
		for(int i=0;i<users.size();i++)
		{
			String send = "";
			String username=users.get(i).getUsername();
			
			List<Travel> travels=travelService.getTravels(username);
			for(int k=0;k<travels.size();k++)
			{
				send=send+travels.get(k).getBeginTravel()+travels.get(k).getTravelPlace()+newline;
			
			}
			
			mail.sendMail(users.get(i).getEmail(), "seyehatlerin", send);
		}
		System.out.println("14: 00'da ve 1. cuma g�n�."+new Date());
	}
	@Scheduled(cron="0 0 14 1 * 6")
	public void monthSaturday(){
		List<User> users =userService.findInformation("Aylik-Cumartesi");
		for(int i=0;i<users.size();i++)
		{
			String send = "";
			String username=users.get(i).getUsername();
			
			List<Travel> travels=travelService.getTravels(username);
			for(int k=0;k<travels.size();k++)
			{
				send=send+travels.get(k).getBeginTravel()+travels.get(k).getTravelPlace()+newline;
			
			}
			
			mail.sendMail(users.get(i).getEmail(), "seyehatlerin", send);
		}
		System.out.println("14: 00'da ve 1. cumartesi g�n�."+new Date());
	}
	@Scheduled(cron="0 0 14 1 * 0")
	public void monthSunday(){
		List<User> users =userService.findInformation("Aylik-Pazar");
		for(int i=0;i<users.size();i++)
		{
			String send = "";
			String username=users.get(i).getUsername();
			
			List<Travel> travels=travelService.getTravels(username);
			for(int k=0;k<travels.size();k++)
			{
				send=send+travels.get(k).getBeginTravel()+travels.get(k).getTravelPlace()+newline;
			
			}
			
			mail.sendMail(users.get(i).getEmail(), "seyehatlerin", send);
		}
		System.out.println("14: 00'da ve 1. pazar g�n�."+new Date());
	}
	//her hafta �al��acak olan job lar
	
	@Scheduled(cron="0 0 14 * * 1")
	public void weekMonday(){
		List<User> users =userService.findInformation("Haftal�k-Pazartesi");
		for(int i=0;i<users.size();i++)
		{
			String send = "";
			String username=users.get(i).getUsername();
			
			List<Travel> travels=travelService.getTravels(username);
			for(int k=0;k<travels.size();k++)
			{
				send=send+travels.get(k).getBeginTravel()+travels.get(k).getTravelPlace()+newline;
			
			}
			
			mail.sendMail(users.get(i).getEmail(), "seyehatlerin", send);
		}
		System.out.println("Pazartesi g�n� saat 14: 00te"+new Date());
	}
	@Scheduled(cron="0 0 14 * * 2")
	public void weekTuesday(){
		List<User> users =userService.findInformation("Haftalik-Sali");
		for(int i=0;i<users.size();i++)
		{
			String send = "";
			String username=users.get(i).getUsername();
			
			List<Travel> travels=travelService.getTravels(username);
			for(int k=0;k<travels.size();k++)
			{
				send=send+travels.get(k).getBeginTravel()+travels.get(k).getTravelPlace()+newline;
			
			}
			
			mail.sendMail(users.get(i).getEmail(), "seyehatlerin", send);
		}
		System.out.println("Sali g�n� saat 14: 00te"+new Date());
	}
	@Scheduled(cron="0 0 14 * * 3")
	public void weekWednesday(){
		List<User> users =userService.findInformation("Haftalik-�ar?amba");
		for(int i=0;i<users.size();i++)
		{
			String send = "";
			String username=users.get(i).getUsername();
			
			List<Travel> travels=travelService.getTravels(username);
			for(int k=0;k<travels.size();k++)
			{
				send=send+travels.get(k).getBeginTravel()+travels.get(k).getTravelPlace()+newline;
			
			}
			
			mail.sendMail(users.get(i).getEmail(), "seyehatlerin", send);
		}
		System.out.println("�ar�amba g�n� saat 14: 00te"+new Date());
	}
	@Scheduled(cron="0 0 14 * * 4")
	public void weekThursday(){
		List<User> users =userService.findInformation("Haftalik-Per?embe");
		for(int i=0;i<users.size();i++)
		{
			String send = "";
			String username=users.get(i).getUsername();
			
			List<Travel> travels=travelService.getTravels(username);
			for(int k=0;k<travels.size();k++)
			{
				send=send+travels.get(k).getBeginTravel()+travels.get(k).getTravelPlace()+newline;
			
			}
			
			mail.sendMail(users.get(i).getEmail(), "seyehatlerin", send);
		}
		System.out.println("Per?embe g�n� saat 14: 00te"+new Date());
	}
	@Scheduled(cron="0 0 14 * * 5")
	public void weekFriday(){
		List<User> users =userService.findInformation("Haftalik-Cuma");
		for(int i=0;i<users.size();i++)
		{
			String send = "";
			String username=users.get(i).getUsername();
			
			List<Travel> travels=travelService.getTravels(username);
			for(int k=0;k<travels.size();k++)
			{
				send=send+travels.get(k).getBeginTravel()+travels.get(k).getTravelPlace()+newline;
			
			}
			
			mail.sendMail(users.get(i).getEmail(), "seyehatlerin", send);
		}
		System.out.println("Cuma g�n� saat 14: 00te"+new Date());
	}
	@Scheduled(cron="0 0 14 * * 6")
	public void weekSaturday(){
		List<User> users =userService.findInformation("Haftalik-Cumartesi");
		for(int i=0;i<users.size();i++)
		{
			String send = "";
			String username=users.get(i).getUsername();
			
			List<Travel> travels=travelService.getTravels(username);
			for(int k=0;k<travels.size();k++)
			{
				send=send+travels.get(k).getBeginTravel()+travels.get(k).getTravelPlace()+newline;
			
			}
			
			mail.sendMail(users.get(i).getEmail(), "seyehatlerin", send);
		}
		System.out.println("Cumartesi g�n� saat 14: 00te"+new Date());
	}
	@Scheduled(cron="0 0 14 * * 0")
	public void weekSunday(){
		List<User> users =userService.findInformation("Haftalik-Pazar");
		for(int i=0;i<users.size();i++)
		{
			String send = "";
			String username=users.get(i).getUsername();
			
			List<Travel> travels=travelService.getTravels(username);
			for(int k=0;k<travels.size();k++)
			{
				send=send+travels.get(k).getBeginTravel()+travels.get(k).getTravelPlace()+newline;
			
			}
			
			mail.sendMail(users.get(i).getEmail(), "seyehatlerin", send);
		}
		System.out.println("Pazar g�n� saat 14: 00te"+new Date());
	}

}
