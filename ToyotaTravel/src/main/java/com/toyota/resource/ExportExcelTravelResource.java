package com.toyota.resource;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.util.*;


import com.toyota.service.TravelService;
import com.toyota.model.Travel;
@Component
@Path("/export")
public class ExportExcelTravelResource {

	@Autowired
	private TravelService travelService;
	
	
	    @GET
	    @Produces("application/vnd.ms-excel")
	    public Response ExportExcel() {
		 
	    	final File file = new File("seyahatler.xls");
	    	
	    	Locale locale = new Locale("tr", "TR");
	    	XSSFWorkbook workbook = new XSSFWorkbook();
	    	List<Travel> travels=travelService.adminAll();
	    	
	    	 List<XSSFSheet> sheets = new ArrayList<XSSFSheet>();
	    	 sheets.add(workbook.createSheet("Seyehatler"));
	    	 List<TreeMap<String, Object[]>> datas = new ArrayList<TreeMap<String, Object[]>>();
	    	 
	    	 datas.add(new TreeMap<String, Object[]>());
	    	 
	    	 datas.get(0).put("1", new Object[]{"department", "departmentManager", "username",
	                 "beginTravel", "endTravel", "travelPlace","goPurpose","projectCode","travelCost"});
	    	 
	    	 for (int i = 0; i < travels.size(); i++) {
	             datas.get(0).put(String.valueOf((i+4)), new Object[]{
	            		 
	            		 travels.get(i).getDepartment(),
	            		 travels.get(i).getDepartmentManager(),
	            		 travels.get(i).getUsername(),
	            		 DateFormat.getDateInstance(DateFormat.DATE_FIELD).format(travels.get(i).getBeginTravel()),
	            		 DateFormat.getDateInstance(DateFormat.DATE_FIELD).format(travels.get(i).getEndTravel()),
	            		// travels.get(i).getBeginTravel(),
	            		// travels.get(i).getEndTravel(),
	            		 travels.get(i).getTravelPlace(),
	            		 travels.get(i).getGoPurpose(),
	            		 travels.get(i).getProjectCode(),
	            		 String.valueOf(travels.get(i).getTravelCost()),
	            		 
	            		 
	            		 
	            		 });
	         }
	    	 
	    	//Bütün keyset listesi dolaşılarak içindeki veriler excel dosyasına yazılıyor.
	         for (int i = 0; i < sheets.size(); i++) {
	             Set<String> keyset = datas.get(i).keySet();
	             int rownum = 0;
	             for (String key : keyset) {
	                 Row row = sheets.get(i).createRow(rownum++);
	                 Object[] objArr = datas.get(i).get(key);
	                 int cellnum = 0;
	                 for (Object obj : objArr) {
	                     Cell cell = row.createCell(cellnum++);
	                     sheets.get(i).autoSizeColumn(cell.getColumnIndex());
	                     if (obj instanceof String)
	                         cell.setCellValue((String) obj);
	                     else if (obj instanceof Integer)
	                         cell.setCellValue((Integer) obj);
	                 }
	             }
	         }
	    	 
	    	 try {
	             //Tanımlanan dosya için diske erişim sağlanıyor.
	             FileOutputStream out = new FileOutputStream(file);
	             //Veriler dosyaya yazılıyor.
	             workbook.write(out);
	             out.close();
	         } catch (Exception e) {
	             e.printStackTrace();
	         }
	         //Başarılı cevabı gönderiliyor ve dosya response'un entity'sine set ediliyor.
	         Response.ResponseBuilder responseBuilder = Response.ok(file);
	         //Kullanıcıya gönderilen dosya ismi belirleniyor.
	         responseBuilder.header("Content-Disposition", "attachment; filename=seyahatler.xls");

	         Timer timer = new Timer();
	         timer.schedule(new TimerTask() {
	             @Override
	             public void run() {
	                 file.delete();
	             }
	         }, 100000);

	         return responseBuilder.build();
		 
		 
		 
		 
		 
	 }
	
	
}
