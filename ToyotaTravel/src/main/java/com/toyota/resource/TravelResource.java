package com.toyota.resource;

import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.toyota.model.Travel;
import com.toyota.security.CustomAuthenticationProvider;
import com.toyota.service.TravelService;

@Component
@Path("/travel")
public class TravelResource {
	

	
	@Autowired
	private TravelService travelService;
	
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	 public Travel save(Travel travel)
	 {
		System.out.print("�alistii save metodu");
		travelService.save(travel);
		 
		 return travel;
	 }


	@DELETE
	@Path("/{travelId}")
	@Produces("text/plain")
	public int delete(@PathParam("travelId") int travelId)
	{   travelService.delete(travelId);
		
		return travelId;
		
	}


	@PUT
	@Path("/{travelId}")
	@Consumes("application/json")
	@Produces("application/json")
	public void edit(@PathParam("travelId") int travelId,Travel travel)
	{
		
		travelService.edit(travel);
	   
	   
		
	}

	@GET
	@Produces("application/json")
	public List<Travel> all()
	{
	return travelService.all();	
	}

	@GET
	@Path("/{travelId}")
	@Produces("application/json")
	@Consumes("application/json")
	public Travel findId(@PathParam("travelId") int travelId){
		
		return travelService.findId(travelId);
	}
	
	
	 @GET
	 @Path("/search/{beginDate}/{endDate}")
	 @Produces("application/json")
	 public List<Travel> searchTravel(@PathParam("beginDate") String  beginDate,@PathParam("endDate") String endDate){
		 
		 
		 return travelService.searchTravel(beginDate, endDate);
		 
	 }
	 
	
             
	 
}
