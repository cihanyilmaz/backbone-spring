package com.toyota.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.toyota.dao.UserDao;
import com.toyota.model.User;

//import javax.transaction.Transactional;

@Service
@Transactional
public class UserService {
	
	@Autowired
	private UserDao userDao;
	
	public User save(User user)
	{
		return userDao.save(user);
		
		
		
	}
	
	public void delete(int id)
	{
		
		userDao.delete(id);
		
		
		
	}
	
	public void edit(User user)
	{
		userDao.edit(user);
	
		
	}
	
	public List<User> all()
	{
		
		return userDao.all();
		
	}
	
	public User findId(int id){
		
		return userDao.findId(id);
		
	}
	
	public User login(String username){
		
		return userDao.login(username);
		
		
	}
	public List<User> findInformation(String time){
		
		return userDao.findInformation(time);
		
	}
	public List<User> controlUsername(String username){
		
		return userDao.controlUsername(username);
		
	}


	
}
	
	

