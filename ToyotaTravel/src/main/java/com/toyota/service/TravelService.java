package com.toyota.service;

import java.util.Date;
import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Transactional;
import com.toyota.model.Travel;
import com.toyota.dao.TravelDao;



@Service
@Transactional
public class TravelService {

	@Autowired
	private TravelDao travelDao;
	
	public Travel save(Travel travel)
	{
		return travelDao.save(travel);
		
		
		
	}
	
	public void delete(int id)
	{
		
		travelDao.delete(id);
		
		
		
	}
	
	public void edit(Travel travel)
	{
		travelDao.edit(travel);
	
		
	}
	
	public List<Travel> all()
	{
		
		return travelDao.all();
		
	}
	
	public Travel findId(int id){
		
		return travelDao.findId(id);
		
	}
	
	public List<Travel> searchTravel(String beginDate,String endDate){
		
		
		return travelDao.searchTravel(beginDate, endDate);
	}
	
	public List<Travel> adminAll()
	{	
		return travelDao.adminAll();
		
		
	}
	
    public List<Travel> searchAdminTravel(String beginDate,String endDate){
		
		
		return travelDao.searchAdminTravel(beginDate, endDate);
	}
    public List<Travel> searchUsername(String username){
    	
    	
    	return travelDao.searchUsername(username);
    }
    public List<Travel> getTravels(String username) {
    	
    	return travelDao.getTravels(username);
    	
    }
    
  
    
    
	
	
}

