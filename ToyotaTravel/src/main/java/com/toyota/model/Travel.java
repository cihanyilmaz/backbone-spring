package com.toyota.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Travel {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String department;
	private String departmentManager;
	private String username;
	
	@Temporal(TemporalType.DATE)
	private Date beginTravel;
	
	@Temporal(TemporalType.DATE)
	private Date endTravel;
	
	private String travelPlace;
	private String goPurpose;
	private String projectCode;
	private float travelCost;
	
	//----------------------------------------------------------
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getDepartmentManager() {
		return departmentManager;
	}
	public void setDepartmentManager(String departmentManager) {
		this.departmentManager = departmentManager;
	}
	public Date getBeginTravel() {
		return beginTravel;
	}
	public void setBeginTravel(Date beginTravel) {
		this.beginTravel = beginTravel;
	}
	public Date getEndTravel() {
		return endTravel;
	}
	public void setEndTravel(Date endTravel) {
		this.endTravel = endTravel;
	}
	public String getTravelPlace() {
		return travelPlace;
	}
	public void setTravelPlace(String travelPlace) {
		this.travelPlace = travelPlace;
	}
	public String getGoPurpose() {
		return goPurpose;
	}
	public void setGoPurpose(String goPurpose) {
		this.goPurpose = goPurpose;
	}
	public String getProjectCode() {
		return projectCode;
	}
	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}
	public float getTravelCost() {
		return travelCost;
	}
	public void setTravelCost(float travelCost) {
		this.travelCost = travelCost;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	
	
}
