package com.toyota.mail;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class MailMail
{
	@Autowired
	private JavaMailSender mailSender;  

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	public void sendMail(String to, String subject, String msg) {
		
		   try{  
		        MimeMessage message = mailSender.createMimeMessage();  
		  
		        MimeMessageHelper helper = new MimeMessageHelper(message, true);  
		        helper.setFrom("cihan.yilmazz144@gmail.com");
		        helper.setTo(to);  
		        helper.setSubject(subject);  
		        helper.setText(msg);  
		  
		       
		  
		        mailSender.send(message);  
		        }catch(MessagingException e){e.printStackTrace();}  
		    }  
	}


