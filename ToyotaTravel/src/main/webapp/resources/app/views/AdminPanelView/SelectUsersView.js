define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/UserModel',
    'app/collections/AdminCollections/UsersCollection',
    'text!app/templates/AdminPanel/SelectUsersTemplate.html',


], function($, _,Backbone,Handlebars,UserModel,UserCollection,SelectUsersTemplate){

            var userCollection=new UserCollection();

            var SelectUsersList=Backbone.View.extend({
            render:function (userCollection) {

                var template = Handlebars.compile(SelectUsersTemplate);
                var myHtml = template({users: userCollection.toJSON()});
                $("#travelUsers").html(myHtml);
                $("#homeUsers").html(myHtml);


            }



            });


            return {
                userCollection:userCollection,
                SelectUsersList:SelectUsersList

            }




});
