define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/UserModel',
    'text!app/templates/AdminPanel/EditAdminTemplate.html',


], function($, _,Backbone,Handlebars,UserModel,EditAdminTemplate){


    var editAdminView=Backbone.View.extend({
        el:'.page',
        model:UserModel,
        events:{

            'click #editAdmin':'editAdmin',
            'click #notEditAdmin':'notEditAdmin',
            'change #editAdminkapali':'close',
            'change #editAdminacik':'open',
            'change #editAdminaylik':'changeMonth',
            'change #editAdminhaftalik':'changeWeek',




        },
        changeMonth:function () {

            $("#editAdminhaftalik").prop( "checked", false );
        },
        changeWeek:function () {

            $("#editAdminaylik").prop( "checked", false );
        },
        close:function () {

        debugger
            $("#ac").prop( "checked", false );
            $("#editAdminacik").toggle();
            return false;



        },
        open:function () {
            debugger
            $("#kapa").prop( "checked", false );
            $("#editAdminkapali").toggle();

            $("#seyahat").toggle();
            $("#editAdminaylik").prop( "checked", true );
            return false;


        },
        editAdmin:function () {
            var informationSystem;
            var user =new UserModel({id:$("#id").val()});

            var newValues=new UserModel();
            newValues.set("role",$("#role").val());
            newValues.set("username",$("#username").val());
            newValues.set("sicilNo",$("#sicilNo").val());
            newValues.set("password",$("#password").val());
            newValues.set("department",$("#department").val());
            newValues.set("departmentManager",$("#departmentManager").val());

            if(!newValues.isValid())
            {
                $("." + newValues.validationError).tooltip("show");


            }else{
                user.fetch({
                    success:function (m_user) {



                        m_user.set("role",$("#role").val());
                        m_user.set("username",$("#username").val());
                        m_user.set("sicilNo",$("#sicilNo").val());
                        m_user.set("password",$("#password").val());
                        m_user.set("department",$("#department").val());
                        m_user.set("departmentManager",$("#departmentManager").val());

                        if(!$("#kapa").is(':checked') && !$("#ac").is(':checked') )
                            informationSystem=m_user.toJSON().informationSystem;



                        if($("#kapa").is(':checked'))
                        {
                            informationSystem=$("#kapa").val();

                        }
                        else if($("#ac").is(':checked')){

                            if($("#editAdminaylik").is(':checked')){



                                informationSystem= $("#editAdminaylik").val()+$("#gunler").val();


                            }
                            else if($("#editAdminhaftalik").is(':checked')){


                                informationSystem=$("#editAdminhaftalik").val()+$("#gunler").val();


                            }


                        }




                        user.set('informationSystem',informationSystem);

                        m_user.save();
                        window.location.href = '#/users';

                    }

                });

            }




            return false;




        },
        notEditAdmin:function () {

            window.location.href = '#/users';


        },
        render:function () {

        debugger
                var that = this;
                var template = Handlebars.compile(EditAdminTemplate);
                var myHtml = template({m_user:that.model.toJSON()});
                that.$el.html(myHtml);
                return this;





        }



    });



    return editAdminView;

});

