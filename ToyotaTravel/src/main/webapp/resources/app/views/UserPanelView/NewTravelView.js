define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/TravelModel',
    'text!app/templates/UserPanel/NewTravelTemplate.html',
    'app/views/UserPanelView/HomeView',
    'app/models/LoginModel'

], function($, _,Backbone,Handlebars,TravelModel,NewTravelTemplate,HomeView,LoginModel){


    var NewTravelView=Backbone.View.extend({
        el:'.page',
        events:{
            'click #saveTravelUser':'saveTravel',
            'click #notSaveTravelUser':'notSaveTravel'


        },
        saveTravel:function () {
        	
        	 var travel =new TravelModel();
            var loginModel=new LoginModel();
            loginModel.fetch({
                success:function (login) {

                   

                    travel.set("department",login.toJSON().department);
                    travel.set('departmentManager',login.toJSON().departmentManager);
                    travel.set("username",login.toJSON().username);
                    travel.set("travelPlace",$("#travelPlace").val());
                    travel.set("goPurpose",$("#goPurpose").val());
                    travel.set("projectCode",$("#projectCode").val());
                    travel.set("beginTravel",$("#beginTravel").val());
                    travel.set("endTravel",$("#endTravel").val());

                    if(!travel.isValid())
                    {
                        if(travel.validationError=="begin")
                        {
                            $("#beginTravel").attr("title","Başlangıç tarihi, Seyehat Sonu tarihinden büyük olamaz!!");
                            $("#beginTravel").tooltip("show");
                            $("#beginTravel").attr("title", "Boş Geçilemez!!");

                        }
                        $("." + travel.validationError).tooltip("show");


                        setTimeout(function () {

                            $("#beginTravel").tooltip("destroy");
                        }, 2000);
                        return false;


                    }else{
                        debugger
                        travel.save();
                        window.location.href='#/userPanel';
                       // Backbone.history.navigate('userPanel');
                        //window.location.reload();
                        return false;


                    }


                    return false;

                }


            });


            return false;



        },
        notSaveTravel:function () {

            window.location.href='#/userPanel';

        },
        render: function () {
            this.$el.html(NewTravelTemplate);
            return this;
        }



    });




    return NewTravelView;







});
