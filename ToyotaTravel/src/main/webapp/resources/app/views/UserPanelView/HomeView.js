define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/TravelModel',
    'text!app/templates/UserPanel/HomeTemplate.html',
    'text!app/templates/UserPanel/HomeListTemplate.html',
    'app/collections/TravelSearchCollection',
    'app/views/EmptyTravelView'

], function($, _,Backbone,Handlebars,TravelModel, HomeTemplate,HomeListTemplate,TravelSearchCollection,EmptyTravelView){

    var HomeEventView=Backbone.View.extend({
        tagName:'tr',
        model:TravelModel,
        events: {
            'click .editTravel': 'editTravel',
            'click .updateTravel': 'updateTravel',
            'click .cancelEdit': 'cancelEdit',
            'click .deleteTravel': 'deleteTravel',
            'mouseover .resultRow':'changeColor',
            'mouseout .resultRow':'notChangeColor'
        },
        notChangeColor:function () {
            this.$el.css('color', 'black');

        },
        changeColor:function () {

            this.$el.css('color', 'red');
        },
        editTravel: function () {
            this.$el.addClass("info");
            this.$el.find("input").show().focus();
            this.$el.find("span").hide();
            $(".editTravel").hide();
            $(".deleteTravel").hide();
           // this.$el.find(".editTravel").hide();
           // this.$el.find(".deleteTravel").hide();

            this.$el.find(".updateTravel").show();
            this.$el.find(".cancelEdit").show();
        },
        cancelEdit: function () {
            this.$el.removeClass("info");
            $(".editTravel").show();
            $(".deleteTravel").show();
            this.render();
        },
        updateTravel: function () {
            debugger
           var department = this.$el.find("#department").val();
            //var department=$("#department").val(); bunla aynı şey
            var departmentManager = this.$el.find("#departmentManager").val();
            var beginTravel = this.$el.find("#beginTravel").val();
            var endTravel = this.$el.find("#endTravel").val();
            var travelPlace = this.$el.find("#travelPlace").val();
            var goPurpose = this.$el.find("#goPurpose").val();
            var projectCode = this.$el.find("#projectCode").val();
            var travelCost = this.$el.find("#travelCost").val();

            var newValues=new TravelModel();
            newValues.set("department",department);
            newValues.set("departmentManager",departmentManager);
            newValues.set("beginTravel",beginTravel);
            newValues.set("endTravel",endTravel);
            newValues.set("travelPlace",travelPlace);
            newValues.set("goPurpose",goPurpose);
            newValues.set("projectCode",projectCode);


            this.$el.removeClass("info");
            if (!newValues.isValid()){
                if(newValues.validationError=="begin")
                {
                    $(".beginTravel").attr("title","Başlangıç tarihi, Seyehat Sonu tarihinden büyük olamaz!!");
                    $(".beginTravel").tooltip("show");
                    $(".beginTravel").attr("title", "Boş Geçilemez!!");
                    return false;
                }
                $("." + newValues.validationError).tooltip("show");
                setTimeout(function () {
                    $("." + newValues.validationError).tooltip("destroy");
                }, 1000);

            }else{

                this.$el.removeClass("info");
                this.model.set("department", department);
                this.model.set("departmentManager", departmentManager);
                this.model.set("beginTravel", beginTravel);
                this.model.set("endTravel", endTravel);
                this.model.set("travelPlace",travelPlace);
                this.model.set("goPurpose", goPurpose);
                this.model.set("projectCode", projectCode);


                this.model.save();




            }
            $(".editTravel").show();
            $(".deleteTravel").show();


            this.render();
        },
        deleteTravel: function () {

            if(uyar())
            {
                this.$el.find(".deleteTravel").addClass("disabled");
                var that = this;

                this.model.destroy({
                    wait: true,
                    success: function () {
                        that.remove();

                    },

                }); // HTTP DELETE

            }
            else{

                return false;


            }

        },
        render: function () {
            var that = this;
            var template = Handlebars.compile(HomeListTemplate);
            var myHtml = template(that.model.toJSON());
            that.$el.html(myHtml);
            return this;
        }




    });
    var HomeView=Backbone.View.extend({

        el: $(".page"),
        events:{
            'click #searchTravelUser': 'search',
            'click #more':'more',


        },
        more:function () {

           // var topla=72;//9 yazınca 3lü atıyor

            var toplaUser=2;
            parseInt(sessionStorage.setItem("degeri",toplaUser+  parseInt(sessionStorage.getItem("degeri"))))

            $("#travelList tr:lt("+sessionStorage.getItem("degeri")+")").show();

        },
        search:function () {

            if($("#beginDate").val()=='' && $("#endDate").val()=='' )
                location.reload();
            if($("#beginDate").val()=='')
            {
                $(".userBeginDate").tooltip("show");
                setTimeout(function () {
                    $(".userBeginDate").tooltip("destroy");
                }, 2000);
                return false;
            }
            if($("#endDate").val()=='')
            {
                $(".userEndDate").tooltip("show");
                return false;
            }

            if($("#beginDate").val()>$("#endDate").val())
            {
                $(".userBeginDate").attr("title","Başlangıç Tarihi Bitiş tarihinden Büyük Olamaz!!");
                $(".userBeginDate").tooltip("show");
                $(".userBeginDate").attr("title","Boş Geçilemez!!");
                setTimeout(function () {
                    $(".userBeginDate").tooltip("destroy");
                }, 2000);

                return false;
            }




            var beginDate = $("#beginDate").val();
            var endDate = $("#endDate").val();

            var search = new TravelSearchCollection( {
                beginDate: beginDate,
                endDate: endDate,

            });

            search.fetch({

                success: function (m_travels) {

                    $(".resultRow").parent().empty();

                    if (m_travels.size() == 0) {
                        var emptyTravelView=new EmptyTravelView();

                        emptyTravelView.model={column:8};

                        $("#travelList").append(emptyTravelView.render().el);


                    }else{

                        m_travels.each(function (m_travel) {

                            var searchView = new HomeEventView();
                            searchView.model = m_travel;
                            $("#travelList").append(searchView.render().el);
                        });




                    }


                }
            });


        },

        render: function () {
            this.$el.html(HomeTemplate);
            return this;
        }
    });


    return {
        HomeEventView: HomeEventView,
        HomeView: HomeView
    };

});

