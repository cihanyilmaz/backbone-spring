define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'app/models/UserModel',
    'text!app/templates/UserPanel/EditUserTemplate.html',
    'app/views/AdminPanelView/UsersView'



], function($, _,Backbone,Handlebars,UserModel,EditUserTemplate,UsersView){


 var editUserView=Backbone.View.extend({
    el:'.page',
     model:UserModel,
     events:{

        'click #editUser':'editUser',
        'click #notEditUser':'notEditUser',
         'change #editUserkapali':'close',
         'change #editUseracik':'open',
         'change #aylik':'changeMonth',
         'change #haftalik':'changeWeek',




     },
     changeWeek:function () {
         $("#aylik").prop( "checked", false );
     },
     changeMonth:function () {
         $("#haftalik").prop( "checked", false );
     },
     close:function () {
debugger

         $("#ac").prop( "checked", false );
         $("#editUseracik").toggle();
         return false;


     },
     open:function () {
debugger
         $("#kapa").prop( "checked", false );
         $("#editUserkapali").toggle();

         $("#seyahat").toggle();
         $("#aylik").prop( "checked", true );
         return false;

     },
     editUser:function () {

        var informationSystem;
        var user =new UserModel({id:$("#id").val()});
         var newValues=new UserModel();
         newValues.set("username",$("#username").val());
         newValues.set("sicilNo",$("#sicilNo").val());
         newValues.set("password",$("#password").val());
         newValues.set("department",$("#department").val());
         newValues.set("departmentManager",$("#departmentManager").val());
         if(!newValues.isValid())
         {

             $("." + newValues.validationError).tooltip("show");


         }else{

             user.fetch({
                 success:function (m_user) {

                     m_user.set("username",$("#username").val());
                     m_user.set("sicilNo",$("#sicilNo").val());
                     m_user.set("password",$("#password").val());
                     m_user.set("department",$("#department").val());
                     m_user.set("departmentManager",$("#departmentManager").val());

                     if(!$("#kapa").is(':checked') && !$("#ac").is(':checked') )
                         informationSystem=m_user.toJSON().informationSystem;



                     if($("#kapa").is(':checked'))
                     {
                         informationSystem=$("#kapa").val();

                     }
                     else if($("#ac").is(':checked')){

                         if($("#aylik").is(':checked')){



                             informationSystem= $("#aylik").val()+$("#gunler").val();


                         }
                         else if($("#haftalik").is(':checked')){


                             informationSystem=$("#haftalik").val()+$("#gunler").val();


                         }


                     }




                     user.set('informationSystem',informationSystem);
                     m_user.save();
                     window.location.href = '#/userPanel';

                 }

             });

         }



         return false;




     },
     notEditUser:function () {

         window.location.href = '#/userPanel';


     },
    render:function () {

        var that = this;
        var template = Handlebars.compile(EditUserTemplate);
        var myHtml = template(that.model.toJSON());
        that.$el.html(myHtml);
        return this;


    }



 });



return editUserView;

});
