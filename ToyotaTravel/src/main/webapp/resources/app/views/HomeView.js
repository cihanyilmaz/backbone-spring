define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'text!app/templates/HomeTemplate.html'

], function($, _,Backbone,Handlebars,HomeTemplate) {

    var HomeView = Backbone.View.extend({

            el: '.page',

            render: function () {

                this.$el.html(HomeTemplate);
                return this;

            }


        }
    );
    return HomeView;


});
